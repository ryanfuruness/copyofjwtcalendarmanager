'use strict';

var fs = require('fs');
var jwt = require('jsonwebtoken');

module.exports = verifyToken;

/**
 * Verifies the token from the authorization header
 * @param authorizationHeader the header to get the token from
 * @param publicKey the public key to verify the token with
 * @param callback callback function with params err, decoded token
 */
function verifyToken(authorizationHeader, publicKey, callback) {
    console.log("verifying token");
  _getAuthorizationToken(authorizationHeader, function (err, token) {
    if (err) {
      return callback(err);
    }

    _verifyAuthorization(token, publicKey, callback);
  });
}

/**
 * Gets the authorization token from an authorization header
 * @param header the header to get the token from
 * @param callback function with params err, JWT
 * @private
 */
function _getAuthorizationToken(header, callback) {
  if (!header) {
    return callback('Unauthorized: Missing authorization header');
  }

  // Authorization: Bearer <JWT>
  var authorizationParts = header.match(/\S+/g);
  var authorization = authorizationParts[1];

  if (!authorization) {
    return callback('Unauthorized: Invalid authorization header');
  }

  callback(null, authorization);
}

/**
 * Verifies the JWT for authenticity
 * @param token the token to verify
 * @param publicKey the public key
 * @param callback function with params err, decodedToken
 * @private
 */
function _verifyAuthorization(token, publicKey, callback) {
  jwt.verify(token, publicKey, {
    algorithms: ['RS256']
  }, function (err, decoded) {
    if (err) {
      return callback('Unauthorized: ' + err.message, decoded);
    }

    callback(null, decoded);
  });
}
