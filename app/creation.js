'use strict';

var jwt = require('jsonwebtoken');
var _ = require('lodash');

module.exports = createToken;

/**
 * Creates the JWT token
 * @param payload the payload to sign
 * @param privateKey the private key
 * @param config the configuration options
 * @param callback callback function with params err, token
 */
function createToken(payload, privateKey, config, callback) {
  var extendedConfig = _.extend({ algorithm: 'RS256',
    expiresIn: "10000",
    audience: 'https://asti-usa.com',
    issuer: 'https://asti-usa.com' }, config);

  var token = jwt.sign(payload, privateKey, extendedConfig);

  callback(null, token);
  
  console.log("creating token");
}

