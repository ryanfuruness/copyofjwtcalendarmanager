'use strict';

var _ = require('lodash');
var creation = require('./app/creation.js');
var verification = require('./app/verification.js');

var publicKey = null;
var privateKey = null;
var config = null;

module.exports = function (pubkey, privkey, config) {
  setPubkey(pubkey);
  setPrivkey(privkey);
  setConfig(config);

  return {
    verifyToken: verifyToken,
    setPubkey:   setPubkey,
    signToken:   signToken,
    setPrivkey:  setPrivkey
  };
};

/**
 * Verifies a given token
 * @param token the token to verify
 * @param callback params err, decodedToken
 */
function verifyToken(token, callback) {
  if (_.isNull(publicKey)) {
    return callback('Public key must be set');
  }

  verification(token, publicKey, callback);
}

/**
 * Signs a given payload
 * @param payload the payload to sign
 * @param callback params err, JWT
 */
function signToken(payload, callback) {
  if (_.isNull(privateKey)) {
    return callback('Private key must be set');
  }

  creation(payload, privateKey, config, callback);
}

/**
 * Sets the path to the pubkey
 * @param pubkey the pubkey
 */
function setPubkey(pubkey) {
  publicKey = pubkey;
}

/**
 * Sets the path to the private key
 * @param privkey the private key
 */
function setPrivkey(privkey) {
  privateKey = privkey;
}

/**
 * Sets the configuration options for the JWT
 * @param con the configuration options
 */
function setConfig(con) {
  config = con;
}
